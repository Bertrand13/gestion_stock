<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ProduitController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index()
    {
        $produits = Product::all();
        return view('produits.listProd', compact('produits'));
    }

    public function create(){
        return view('produits.addProduit');
    }

    public function addProduit(Request $request){
        $produit = new Product();

        $produit->designation=$request['designation'];
        $produit->qteMin=$request['qteMin'];
        $produit->qteStock=$request['qteStock'];
        $produit->prix=$request['prix'];

        $produit->save();
        $produits = Product::all();
        return view('produits.listProd', compact('produits'));
    }
}
