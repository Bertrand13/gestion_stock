<x-app-layout>
  <x-slot name="header">
      <h2 class="font-semibold text-xl text-gray-800 leading-tight">
          {{ __('Liste des utilisateurs') }}
      </h2>
  </x-slot>

  <div class="py-12">
      <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
          <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
              <div class="p-6 bg-white border-b border-gray-200">
                  <table  BORDER="2" style="width: 100%" >
                    <tr>
                      <td>Nom</td>
                      <td>Email</td>
                      <td>Actions</td>
                    </tr>
                    @foreach ($users as $user)
                      <tr>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                          <button>Modifier</button>&nbsp; &nbsp;
                          <form action="/users/{{$user->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit">Supprimer</button>
                          </form>
                        </td>
                      </tr>
                    @endforeach
                  </table>
              </div>
          </div>
      </div>
  </div>
</x-app-layout>
