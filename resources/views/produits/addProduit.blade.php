<x-app-layout>
  <x-slot name="header">
      <h2 class="font-semibold text-xl text-gray-800 leading-tight">
          {{ __('Ajout de produits') }}
      </h2>
  </x-slot>

  <div class="py-12">
      <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
          <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
              <div class="p-6 bg-white border-b border-gray-200">
              <form method="POST" action="{{ route('create') }}">
    @csrf
            <div class="form-group">
                <label for="title">Designation du produit</label>
                <input type="text" class="form-control block mt-1 w-full
                {{ $errors->has('designation') ? 'is-invalid' : ''}} " id="designation" name="designation">
                @if($errors->has('designation'))
                  <span class="invalid-feedback">{{ $errors->first('designation') }}</span>
                @endif
            </div>

            <div class="form-group">
                <label for="title">Quantite Minimum</label>
                <input type="text" class="form-control block mt-1 w-full
                {{ $errors->has('qteMin') ? 'is-invalid' : ''}} " id="qteMin" name="qteMin">
                @if($errors->has('qteMin'))
                  <span class="invalid-feedback">{{ $errors->first('qteMin') }}</span>
                @endif
            </div>

            <div class="form-group">
                <label for="title">Quantite en stock</label>
                <input type="text" class="form-control block mt-1 w-full
                {{ $errors->has('qteStock') ? 'is-invalid' : ''}} " id="qteStock" name="qteStock">
                @if($errors->has('qteStock'))
                  <span class="invalid-feedback">{{ $errors->first('qteStock') }}</span>
                @endif
            </div>

            <div class="form-group">
                <label for="title">Prix</label>
                <input type="text" class="form-control block mt-1 w-full
                {{ $errors->has('prix') ? 'is-invalid' : ''}} " id="prix" name="prix">
                @if($errors->has('prix'))
                  <span class="invalid-feedback">{{ $errors->first('prix') }}</span>
                @endif
            </div>

            <x-button type="submit" class="btn btn-primary block mt-1">Ajouter </x-button>
    </form>
              </div>
          </div>
      </div>
  </div>
</x-app-layout>