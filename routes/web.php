<?php

use App\Http\Controllers\UserController;
use App\Http\Controllers\ProduitController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => ['auth']], function () {
    
    //Route::delete('/users/{id}', [UserController::class, 'destroy'])->name('users.destroy');

    //User's route 
    Route::resource('users', UserController::class);


    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');

    
//Creation de nos routes
Route::get('/listProduit', [ProduitController::class, 'index'])->name('listProd');

Route::get('/AddProduit', [ProduitController::class, 'create'])->name('addProd');


Route::post('/create', [ProduitController::class, 'addProduit'])->name('create');
});



require __DIR__.'/auth.php';
