# Gestion de Stock 🔺


## Installation pour ceux qui n'ont pas le projet en local

* Clone this repo:

```bash
git clone https://marius05@bitbucket.org/Bertrand13/gestion_stock.git
```

* Ouvrir le projet dans vs code 
* installer les dependances

```bash
composer install 
```

* Renommer le fichier .env.example en .env 
* Ouvrir le fichier .env au niveau de DB_DATABASE=laravel remplacer laravel par le nom de la base de donnee que vous avez creer en local

<br>

* Installer les dependances nodes  

```bash
npm install 
```

* Lancer le serveurs nodes 

```bash
npm run dev
```

* Lancer la migration pour creer les tables dans la base de donnees 

```bash
php artisan migrate 
```

* Generer une cle 

```bash
php artisan migrate:key
```

* Lancer lr projet

```bash
php artisan serve 
```

<br>
